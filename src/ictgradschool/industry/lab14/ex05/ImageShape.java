package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;

    private class LoadImages extends SwingWorker<Image, Void> {
        public Image img;
        private URL url;
        int width;
        int height;

        public LoadImages(URL url, int width, int height) {
            this.url = url;
            this.width = width;
            this.height = height;
        }

        @Override
        protected Image doInBackground() throws Exception {
            img = ImageIO.read(url);

                if (width == img.getWidth(null) && height == img.getHeight(null)) {
                    return img;
                } else {
                    this.img = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    img.getHeight(null);
                    return img;
                }
        }

        protected void done() {

            try {
                image = get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
    }



    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);
        LoadImages loader = new LoadImages(url, width, height);
        loader.execute();
        try {
            URI uri = new File("350x150.png").toURI();
            URL url1 = uri.toURL();
            image=  ImageIO.read(url1);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void paint(Painter painter) {

        painter.drawImage(this.image , fX, fY, fWidth, fHeight);

    }
}
