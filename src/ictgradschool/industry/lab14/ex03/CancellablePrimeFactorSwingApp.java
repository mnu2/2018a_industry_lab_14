package ictgradschool.industry.lab14.ex03;

import ictgradschool.industry.lab14.ex02.SingleThreadedPrimeFactorsSwingApp;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class CancellablePrimeFactorSwingApp extends JPanel {

    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _abortBtn;//Button to cancel the computation.
    private JTextArea _factorValues;  // Component to display the result.
    private PrimeFactorisationWorker worker;

    private class PrimeFactorisationWorker extends SwingWorker<List<Long>, Void> {

        protected Long n;

        public PrimeFactorisationWorker(Long n) {
            this.n = n;
        }

        @Override
        protected List<Long> doInBackground() throws Exception {

            List<Long> list = new ArrayList<Long>();
            _abortBtn.setEnabled(true);
            _startBtn.setEnabled(false);
                for (long i = 2; i * i <= n; i++) {
                    if(!isCancelled()) {
                        // If i is a factor of N, repeatedly divide it out
                        while (n % i == 0) {
                            list.add(i);
                            n = n / i;
                        }
                    }
                    else {
                        System.out.println("Process cancelled");
                        _abortBtn.setEnabled(false);
                        _startBtn.setEnabled(true);
                        break;};

            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
                list.add(n);
            }
//            _abortBtn.setEnabled(false);
//            _startBtn.setEnabled(true);
            return list;
        }

        protected void done() {
            try {
                List<Long> list = get();
                for (Long s : list) {
                    _factorValues.append(s + "\n");
                }
                _abortBtn.setEnabled(false);
                _startBtn.setEnabled(true);
            } catch (Exception e) {
            }



            setCursor(Cursor.getDefaultCursor());
        }
    }

    public CancellablePrimeFactorSwingApp() {
        // Create the GUI components.

        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _abortBtn = new JButton("Abort");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                    worker = new PrimeFactorisationWorker(n);
                    worker.execute();
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }

                // Disable the Start button until the result of the calculation is known.


                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                // Start the computation in the Event Dispatch thread.


                // Re-enable the Start button.

                // Restore the cursor.
                setCursor(Cursor.getDefaultCursor());
            }
        });

        _abortBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                worker.cancel(true);
                //_abortBtn.setEnabled(false);
                //_startBtn.setEnabled(true);
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new CancellablePrimeFactorSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }


}
